# HookBot

HookBot is a simple IRC bot that relays Gitlab web hooks output to IRC server/channel.\
Gitlab web hooks are called when a user make changes to a repository, changes such as committing changes, commenting. working with issues or working with PR.\
GitLab hooks are sent as an http POST request to a selected address in REST api format.

HookBot connects to a pre-defined IRC server/channel whenever there are web hooks calls to take care of and post them to IRC.\
After posting to IRC, the bot idles for threshold time changeable in settings by replying to IRC PING by the server and disconnects if no further hook messages are recieved. Each new message resets the timer for disconnect.

The bot is written and tested using Python 3.

## Dependencies 

* [flask](https://palletsprojects.com/p/flask/) (1.1.X+ recommended)

## Installation 

1. Clone this project.
2. Enable web hooks in Gitlab repository settings, set recieving address, set a token for the web hook call. token works as a password and passed in the http POST header.
3. Change the settings at the top of the `hook_bot.py`.
4. Register a nick for the bot on the IRC server, with a strong password and update `hook_bot.py` with the bot nick and password.

## Running

### debug mode

The script can be run using flask or be called directly. flask call is preferable as it will reload the script when it is changed.

Using flask:

    export FLASK_APP=hook_bot.py
    export FLASK_ENV=development
    flask run --with-threads -p 9756 --host='0.0.0.0' --cert=adhoc

Using the script directly:

    python3 hook_bot.py &
    
### release mode
    
After completing development stage, replace flask with production ready solution.
Flask web server is used for development only and isn't secure as production solutions.

For more info:

    https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/