import _thread
import json
import socket
import ssl
import time

from flask import Flask, request, abort

# Change those settings
cert = ''  # path TODO: make use
cert_key = ''  # path TODO: make use
verbose = True   # print verbose info to std output
dry_run = False  # When true, no irc connection or msgs will be posted.
port = 0  # listening port for GitLab hooks
gitlab_token = 'test'  # Compared to the token in GitLab web hooks and only continue to irc if they match
irc_server = "change me"  # IRC serverm, Example: irc.server.net
irc_port = 7000  # connection port to IRC
channel = "#Testing"  # Channel to each to post IRC messages
bot_nick = "HookBot"  # bot nick
exit_code = "bye " + bot_nick  # bot quit message from IRC server
bot_pass = "bot pass"  # IRC password for the bot nick
socket_timeout = 360  # timeout to wait for IRC msgs, we set in to 360 to allow the ping thread enough time to reply to PING req
last_message = time.time()  # last message time, updated when posting to IRC
threshold = 5 * 60  # idle time to wait for additional input after posting to IRC before disconnecting
hook_url = "/web_hook"  # Url path for the http connection
# End of settings

# Socket for irc connection
irc_conn_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
app = Flask(__name__)
ctx = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
ctx.options &= ~ssl.OP_NO_SSLv3


def is_zeroes_string(string):
    return all([c == '0' for c in string])


def action_to_past(action):
    if action == "open":
        return "opened"
    elif action == "close":
        return "closed"
    elif action == "reopen":
        return "reopened"
    elif action == "update":
        return "updated"
    else:
        return ""


def remove_prefix(s, prefix):
    return s[len(prefix):] if s.startswith(prefix) else s


def ping_thread():
    last_ping = time.time()
    try:
        while 1:
            data = irc_conn_socket.recv(1024).decode("utf-8")
            if data.find('PING') != -1:
                irc_conn_socket.send(bytes('PONG ' + data.split()[1] + '\r\n', "UTF-8"))
                last_ping = time.time()
                if verbose:
                    print("Received PING from " + data.split()[1] + " replied with PONG")
            if (last_ping - last_message) > threshold:
                if verbose:
                    print("No data for threshold time, disconnecting")
                break
    finally:
        disconnect()


def join_channel(chan):
    if verbose:
        print("Trying to join channel: " + chan)
    irc_conn_socket.send(bytes("JOIN " + chan + "\n\r", "UTF-8"))


def send_msg(msg, target=channel):  # sends messages to the target.
    global last_message
    if verbose:
        print("PRIVMSG {} {}\r\n".format(target, msg))
    irc_conn_socket.send(bytes("PRIVMSG {} {}\r\n".format(target, msg), "UTF-8"))
    last_message = time.time()


def disconnect():
    try:
        if verbose:
            print("QUIT " + exit_code + "\r\n")
        irc_conn_socket.send(bytes("QUIT " + exit_code + "\r\n", "UTF-8"))
    finally:
        irc_conn_socket.close()


def connect():
    try:
        global irc_conn_socket
        if verbose:
            print("Creating new connection")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if verbose:
            print("Wrapping with ssl")
        irc_conn_socket = ctx.wrap_socket(sock)
        if verbose:
            print("Connecting")
        irc_conn_socket.connect((irc_server, irc_port))
        irc_conn_socket.settimeout(socket_timeout)
        if verbose:
            print("Connected")
        time.sleep(2)
        irc_conn_socket.send(bytes("USER " + bot_nick + " " + bot_nick + " " + bot_nick + " :Hooker\n", "UTF-8"))
        irc_conn_socket.send(bytes("NICK " + bot_nick + "\n\r", "UTF-8"))
        time.sleep(3)

        if verbose:
            print("Sending identify msg")
        irc_conn_socket.send(bytes("PRIVMSG NICKSERV IDENTIFY {} {}\r\n".format(bot_nick, bot_pass), "UTF-8"))
        time.sleep(2)

        join_channel(channel)

        _thread.start_new_thread(ping_thread, ())
    except Exception as e:
        if verbose:
            print("Failed to process join request" + str(e))


def ship_msg(message):
    try:
        send_msg(message)
    except:
        if verbose:
            print("No connection, connecting")
        try:
            connect()
            time.sleep(1)
            send_msg(message)
        except:
            print("Failed to connect and ship msg")


def process_issue_hook(body, is_merge=False):
    project = body.get("project")
    user = body.get("user")
    attributes = body.get("object_attributes")
    if project is None or user is None or attributes is None:
        if verbose:
            print("Format error in body")
        return

    action = action_to_past(attributes["action"])

    if is_merge:
        issue_type = "merge request"

    else:
        issue_type = "issue"

    out_str = '[\x034{}\x03] {} {} {} \'\x035{}\x03\': \x032{}\x03'.format(project["name"], user["username"],
                                                                           action, issue_type,
                                                                           attributes["title"],
                                                                           attributes["url"])
    if verbose:
        print(out_str)
    if not dry_run:
        ship_msg(out_str)


def process_push_hook(body):
    project = body.get("project")
    user_name = body.get("user_username")
    if project is None or user_name is None:
        if verbose:
            print("Format error in body")
        return
    project = body["project"]
    user_name = body["user_username"]
    project_name = project["name"]

    ref = remove_prefix(body.get('ref', ""), 'refs/heads/')

    commits = body["commits"]
    before = body["before"][0:9]
    after = body["after"][0:9]
    new = ""
    if is_zeroes_string(before):
        new = "new branch "
        before = commits[0]["id"][0:9]

    if before == after:
        url = commits[-1]["url"]
    else:
        url = project["git_http_url"][:-4] + "/compare/" + before + "..." + after

    out_str = '[\x034{}\x03] {} pushed \x02{}\x02 new commits to {}\x035{}\x03: \x032{}\x03'.format(project_name,
                                                                                                    user_name,
                                                                                                    body["total_commits_count"],
                                                                                                    new,
                                                                                                    ref, url)
    if verbose:
        print(out_str)
    if not dry_run:
        ship_msg(out_str)

    for commit in commits:
        numn_files_changed = len(commit["added"]) + len(commit["modified"]) + len(commit["removed"])
        commit_str = '\x034{}\x03/\x035{}\x03 \x0314{}\x03 {} (\x0312{} files\x03): {}'.format(project_name,
                                                                                               ref,
                                                                                               commit["id"][0:9],
                                                                                               user_name,
                                                                                               numn_files_changed,
                                                                                               commit["message"])
        if verbose:
            print(commit_str)
        if not dry_run:
            ship_msg(commit_str)


def process_comment_hook(body):
    project = body.get("project")
    user = body.get("user")
    attributes = body.get("object_attributes")
    if project is None or user is None or attributes is None:
        if verbose:
            print("Format error in body")
        return

    if "commit" in body:
        title = body["commit"]["id"][0:9] + " (" + body["commit"]["author"]["name"] + ")"
    elif "merge_request" in body:
        title = body["merge_request"]["title"]
    elif "issue" in body:
        title = body["issue"]["title"]
    elif "snippet" in body:
        title = body["snippet"]["title"]
    else:
        title = ""

    out_str = '[\x034{}\x03] {} commented on \x035{}\x03 \'\x035{}\x03\': \x032{}\x03'.format(project["name"],
                                                                                              user["username"],
                                                                                              attributes[
                                                                                                  "noteable_type"],
                                                                                              title,
                                                                                              attributes["url"])
    if verbose:
        print(out_str)
    if not dry_run:
        ship_msg(out_str)


@app.route(hook_url, methods=['POST'])
def process_post_request():
    token = request.headers.get('X-Gitlab-Token')
    event = request.headers.get('X-Gitlab-Event')

    if token is None or event is None:
        if verbose:
            print("Invalid header received")
        abort(403)
    if token != gitlab_token:
        if verbose:
            print(token)
            abort(403)
    elif verbose:
        print("Token success")
    body = json.loads(request.data.decode('utf-8'))

    # Clear main thread, we might miss updates otherwise, gitlab retry a few times
    if event == 'Issue Hook':
        _thread.start_new_thread(process_issue_hook, (body, False,))
    elif event == 'Push Hook':
        _thread.start_new_thread(process_push_hook, (body,))
    elif event == 'Note Hook':
        _thread.start_new_thread(process_comment_hook, (body,))
    elif event == 'Merge Request Hook':
        _thread.start_new_thread(process_issue_hook, (body, True,))
    else:
        if verbose:
            print("Invalid hook type")
            print(event)

        abort(403)

    return 'Received !'  # response to your request.


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port, debug=verbose, threaded=True, ssl_context='adhoc')
